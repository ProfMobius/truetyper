An open TrueType Font renderer for Minecraft
 
USAGE GUIDE!

Create a TrueTypeFont object variable using either a resource location to a .ttf or
name of a system font, choose base size and whether or not you want anti aliasing like so:

testFont = FontLoader.createFont(new ResourceLocation("modid", "testfont.ttf"), 24f, false);
testFont2 = FontLoader.loadSystemFont("Arial", 16f, false);



Once the font has been created, to render to screen, simply use the static call:

FontHelper.drawString(String name, float x, float y, TrueTypeFont font, float scaleX, float scaleY, float... rgba)

--

Example:

FontHelper.drawString(windowTitle, posX, posY, testFont, 1f, 1f);

--

gl&hf
 - Mr_okushama